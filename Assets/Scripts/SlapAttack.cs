﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlapAttack : MonoBehaviour, IAttack
{
    public GameObject sword;
    private bool isAttacking = false;
    public float attackTime = 1.0f;

    public void Attack()
    {
        if (!isAttacking)
        {
            sword.SetActive(true);
            isAttacking = true;

            StartCoroutine(ExecuteAttack());
        }
    }

    public IEnumerator ExecuteAttack()
    {
        yield return new WaitForSeconds(attackTime);
        sword.SetActive(false);
        isAttacking = false;
    }
}
