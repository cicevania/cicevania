﻿
public interface IInput
{
    float Horizontal  { get; set; }
    float Vertical  { get; set; }
}