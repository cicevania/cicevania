﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    new private Rigidbody2D rigidbody2D;
    public Transform groundPoint;


    private IInput InputReader { get; set; }
    private IMovement MoveResponse { get; set; }
    private IJumper CurrentJumper { get; set; }
    private IJumper DoubleJumper { get; set; }
    private IAttack AttackResponse { get; set; }
    private IDash DashResponse { get; set; }

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        InputReader = GetComponent<IInput>();
        MoveResponse = GetComponent<IMovement>();
        AttackResponse = GetComponent<IAttack>();
        CurrentJumper = GetComponent<SimpleJumper>();
        DoubleJumper = GetComponent<DoubleJumper>();
        DashResponse = GetComponent<IDash>();

        ((InputReader)InputReader).OnJump += Jump;
        ((InputReader)InputReader).OnAttack += Attack;
        ((InputReader)InputReader).OnDash += Dash;
    }

    private void Dash()
    {
        DashResponse.PerformeDash();
    }

    private void Attack()
    {
        AttackResponse.Attack();
    }

    private void Jump()
    {
        CurrentJumper.Jump();
    }

    public void ChangeJump(IJumper newJumper)
    {
        CurrentJumper = newJumper;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ChangeJump(DoubleJumper);
        }
    }

    private void FixedUpdate()
    {
        rigidbody2D.velocity = MoveResponse.Move(InputReader.Horizontal);
    }
}
