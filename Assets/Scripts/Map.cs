﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public GameObject tutorial, hall, biblioteca, cocina1, cocina2, alcantarilla, cripta, jardines, torreon, boss;
    public Canvas canvas;
    public string estanciaActual;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
            MostrarMapa();
    }

    void MostrarMapa()
    {
        //Primero paramos el tiempo o lo continuamos (si se pulsa de nuevo M)
        if (Time.timeScale == 1)
            Time.timeScale = 0;
        else if (Time.timeScale == 0)
            Time.timeScale = 1;

        GameObject playerEscena = GameObject.Find(estanciaActual);//buscamos donde se encuentra el player
        playerEscena.GetComponent<Renderer>().material.color = Color.green;
    }

    void ActivarEscenario(GameObject escenario)
    {
        escenario.GetComponentInParent<GameObject>().SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.name)
        {
            case "tutorial":{ ActivarEscenario(tutorial);break;}
            case "hall": { ActivarEscenario(hall);break;}
            case "biblioteca": { ActivarEscenario(biblioteca);break;}
            case "cocina1": { ActivarEscenario(cocina1);break;}
            case "cocina2": { ActivarEscenario(cocina1);break;}
            case "alcantarilla": { ActivarEscenario(alcantarilla);break;}
            case "cripta": { ActivarEscenario(cripta);break;}
            case "jardines": { ActivarEscenario(jardines);break;}
            case "torreon": { ActivarEscenario(torreon);break;}
            case "boss": { ActivarEscenario(boss);break;}
        }

        estanciaActual = collision.name;
    }
}
