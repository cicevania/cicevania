﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMoveResponse : MonoBehaviour, IMovement
{
    private Vector2 velocitySmooth;
    private Vector2 targetVelocity;
    private Vector2 velocity;

    public float moveAmount;
    public float smoothVelocity;

    public Vector2 Move(float moveDirection)
    {
        RotatePlayer(moveDirection);

        targetVelocity = new Vector2(moveDirection * Time.fixedDeltaTime * moveAmount, GetComponent<Rigidbody2D>().velocity.y);
        
        velocitySmooth = Vector2.SmoothDamp(GetComponent<Rigidbody2D>().velocity, targetVelocity, ref velocity, smoothVelocity);

        return velocitySmooth;
    }

    private void RotatePlayer(float moveDirection)
    {
        if (moveDirection < 0)
        {
            transform.rotation = Quaternion.Euler(0,180,0);
        }
        else if (moveDirection > 0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
}
