﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Integer Variable")]
public class IntVariable : ScriptableObject
{
    public int value;
}
