﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*Este script se pone en el gameobject llave.
 * El objeto llave debe estar en una capa que solo interactue con el player
 * Al script se le añade un asset (ScriptableObject) que lleva el nombre de la llave*/


public class KeyObject : MonoBehaviour
{
    public BoolVariable onPlayer;

    private void OnTriggerEnter(Collider other)
    { 
         onPlayer.value = true;
    }
}
