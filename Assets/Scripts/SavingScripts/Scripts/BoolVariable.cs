﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Bool Variable"))]
public class BoolVariable : ScriptableObject
{
    public bool value;

    public void SetValue(bool b)
    {
        value = b;
    }

}
