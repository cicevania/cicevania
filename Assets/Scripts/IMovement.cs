﻿using UnityEngine;

public interface IMovement
{
    Vector2 Move(float moveDirection);
}